/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1_3;

/**
 *
 * @author ACER
 */
public final class ProcessStep implements Runnable {
private static final Object lock = new Object();
private static int time = 0;
private final int step; // Do Perform operations when field time
// reaches this value
public ProcessStep(int step) {
this.step = step;
}
@Override public void run() {
try {
synchronized (lock) {
while (time != step) {
lock.wait();
}
// Perform operations
time++;
lock.notify();
}
} catch (InterruptedException ie) {
Thread.currentThread().interrupt(); // Reset interrupted status
}
}
public static void main(String[] args) {
for (int i = 4; i >= 0; i--) {
new Thread(new ProcessStep(i)).start();
}
}
}

/*
Modul 5 - Latihan 1.3
Komen :
Contoh kode noncompliant ini menunjukkan proses yang kompleks dan multi-Pole yang dilakukan oleh beberapa threads. 
Setiap threads mengeksekusi langkah diidentifikasi oleh bidang waktu. 
Setiap thtreads menunggu bidang waktu untuk menunjukkan bahwa itu adalah waktu untuk melakukan langkah yang sesuai.
*/