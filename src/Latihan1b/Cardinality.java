/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1b;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author ACER
 */
public class Cardinality {
 public static int cardinality(Object obj, final Collection col) {
int count = 0;
if (col == null) {
return count;
}
Iterator it = col.iterator();
while (it.hasNext()) {
Object elt = it.next();
if ((null == obj && null == elt) ||
(null != obj && obj.equals(elt))) {
count++;
}
}
return count;
    
}
}


/*Komen
metode cardinality
untuk menentukan berapa banyak objek dalam koleksi null. Namun, karena keanggotaan
dalam koleksi adalah null pointer diperiksa menggunakan ekspresi obj.equals(elt), dereference
dijamin setiap kali obj null dan elt tidak null.
*/    

