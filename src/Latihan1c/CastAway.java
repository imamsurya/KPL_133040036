/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1c;

/**
 *
 * @author ACER
 */
public class CastAway {
public static void workWith(int i) {
// Check whether i is within byte range
if ((i < Byte.MIN_VALUE) || (i > Byte.MAX_VALUE)) {
throw new ArithmeticException("Value is out of range");
}
byte b = (byte) i;
// Work with b
}
}

/*
Komen :
contoh kode ini adalah nilai tipe int dikonversi ke nilai tipe byte
tanpa memeriksa jangkauan. Nilai yang dihasilkan mungkin tak terduga karena nilai awal  adalah di luar
berbagai jenis yang dihasilkan.
*/
    

