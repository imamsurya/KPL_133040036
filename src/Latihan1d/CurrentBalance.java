/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1d;

/**
 *
 * @author ACER
 */
public class CurrentBalance {
    
double currentBalance; // User's cash balance
void doDeposit(String userInput){
double val = 0;
try {
val = Double.valueOf(userInput);
} catch (NumberFormatException e) {
// Handle input format error
}
if (Double.isInfinite(val)){
// Handle infinity error
}
if (Double.isNaN(val)) {
// Handle NaN error
}
if (val >= Double.MAX_VALUE - currentBalance) {
// Handle range error
}
currentBalance += val;
}
}

/*
Komen :
Kode ini akan menghasilkan hasil yang tak terduga ketika nilai istimewa yang dimasukkan untuk val
dan kemudian digunakan dalam perhitungan atau sebagai nilai-nilai kontrol. Pengguna bisa, misalnya,
masukan string tanpa batas atau NaN pada baris perintah, yang akan diurai oleh ganda.
valueOf (String s) ke dalam penggambaran floating-point tanpa batas atau NaN.
*/
