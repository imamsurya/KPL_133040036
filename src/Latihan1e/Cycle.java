/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1e;

/**
 *
 * @author ACER
 */
public class Cycle {
    
private final int balance;
// Random Deposit
private static final int deposit = (int) (Math.random() * 100);
// Inserted after initialization of required fields
private static final Cycle c = new Cycle();
public Cycle() {
// subtract processing fee
balance = deposit - 10;
}
public static void main(String[] args) {
System.out.println("The account balance is: " + c.balance);
}
}

/*Komen :
Kelas Cycle adalah untuk menyatakan sebuah variabel kelas statis pribadi akhir, yang diinisialisasi ke
sebuah instance baru dari kelas Cycle. Initializers statis dijamin untuk dipanggil sekali sebelum
penggunaan pertama dari seorang anggota kelas statis atau invocation pertama konstruktor.

*/
