/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1f;

/**
 *
 * @author ACER
 */
public class DoOperation {
    
 public int doOperation(int exp) throws ArithmeticException {
if ((exp < 0) || (exp >= 32)) {
throw new ArithmeticException("Exponent out of range");
}
// Safely compute 2^exp
int temp = 1 << exp;
// Do other processing
return temp;
}
}

/*
Komen :
Contoh kode noncompliant ini berisi akhiran blok yang menutup objek pembaca.
Programmer salah mengasumsikan bahwa pernyataan dalam akhirnya blok tidak melemparkan
pengecualian dan akibatnya gagal tepat menangani pengecualian yang mungkin timbul.
Metode close() bisa melempar IOException, yang, jika dibuang, akan mencegah eksekusi
Setiap pernyataan berikutnya.
*/
