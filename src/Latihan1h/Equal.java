/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1h;

/**
 *
 * @author ACER
 */
public class Equal {
    
public void exampleEqualOperator(){
Boolean b1 = true;
Boolean b2 = true;
if (b1 == b2) { // Always equal
System.out.println("Always printed");
}
b1 = Boolean.TRUE;
if (b1 == b2) { // Always equal
System.out.println("Always printed");
}
}   
}

/*
Komen :
Dalam contoh noncompliant kode ini, konstruktor untuk kelas Boolean kembali berbeda, baru
benda-benda yang terkandung. Menggunakan operator kesetaraan referensi tempat nilai perbandingan
akan menghasilkan hasil yang tidak diharapkan.
*/

