/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1i;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author ACER
 */
public class GetInteger {
    
 public static long getInteger(DataInputStream is) throws IOException {
return is.readInt() & 0xFFFFFFFFL; // Mask with 32 one-bits
}
}

/*
Komen :
solusi compliant ini memerlukan bahwa nilai-nilai yang membaca adalah 32-bit unsigned integer. Membaca
nilai unsigned integer yang menggunakan metode readInt(). Metode readInt() mengasumsikan
menandatangani nilai-nilai dan mengembalikan int ditandatangani; nilai kembali dikonversi lama dengan tanda
ekstensi.
Sebagai prinsip umum, Anda harus selalu menyadari signedness data Anda ketika membaca.
*/
