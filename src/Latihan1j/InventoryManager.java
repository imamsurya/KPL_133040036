/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1j;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author ACER
 */
public class InventoryManager {
    
private final AtomicInteger itemsInInventory =
new AtomicInteger(100);
public final void nextItem() {
while (true) {
int old = itemsInInventory.get();
if (old == Integer.MAX_VALUE) {
throw new ArithmeticException("Integer overflow");
}
int next = old + 1; // Increment
if (itemsInInventory.compareAndSet(old, next)) {
break;
}
} // End while
} // End nextItem()
}

/*
Komen :
Operasi pada objek dari jenis AtomicInteger menderita dari masalah melimpah yang sama sebagai lain
jenis integer. Solusi umumnya mirip dengan solusi yang sudah disajikan; Namun,
isu-isu concurrency menambahkan komplikasi. Pertama, potensi masalah dengan waktu-dari-cek,
kondisi ras waktu penggunaan (TOCTOU) harus dihindari;
Contoh kode noncompliant ini menggunakan AtomicInteger, yang merupakan bagian dari concurrency
utilitas. Utilitas concurrency kekurangan integer overflow checks
Akibatnya, itemsInInventory dapat membungkus untuk Integer.MIN_VALUE ketika
metode nextItem() dipanggil ketika itemsInInventory == Integer.MAX_VALUE.
*/


