/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1k;

import java.util.Vector;

/**
 *
 * @author ACER
 */
public class MyVector {
    
private int val = 1;
public boolean isEmpty() {
if (val == 1) { // Compares with 1 instead of 0
return true;
} else {
return false;
}
}
// Other functionality is same as java.util.Vector
}
// import java.util.Vector; omitted
public class VectorUser {
public static void main(String[] args) {
Vector v = new Vector();
if (v.isEmpty()) {
System.out.println("Vector is empty");
}
}
}

/*
Komen :
Contoh kode noncompliant ini mengimplementasikan sebuah kelas yang reuses nama kelas
java.util.Vector. untuk memperkenalkan suatu kondisi yang berbeda metode isEmpty() untuk berinteraksi dengan kode warisan asli dengan meng-override method tersebut di java.util.
Vektor. Perilaku tak terduga dapat timbul jika pengelola membingungkan metode isEmpty() dengan
metode java.util.Vector.isEmpty().
*/
