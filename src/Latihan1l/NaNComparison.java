/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1l;

/**
 *
 * @author ACER
 */
public class NaNComparison {
    
  public static void main(String[] args) {
double x = 0.0;
double result = Math.cos(1/x); // Returns NaN when input is infinity
if (Double.isNaN(result)) {
System.out.println("result is NaN");
}
}
}

/*
Komen :
Contoh kode noncompliant ini upaya perbandingan langsung dengan NaN. Sesuai
dengan semantik NaN, Semua perbandingan dengan NaN menghasilkan palsu (dengan the pengecualian dari
! = operator, yang mengembalikan nilai true). Akibatnya, perbandingan ini selalu kembali palsu, dan
"hasil NaN pesan" tidak pernah tercetak.
Solusi ini compliant menggunakan metode Double.isNaN() untuk memeriksa apakah ekspresi
sesuai dengan nilai NaN
*/

