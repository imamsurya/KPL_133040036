/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1m;

/**
 *
 * @author ACER
 */
public class RangeCheck {
    
public static long intRangeCheck(long value) {
if ((value < Integer.MIN_VALUE) || (value > Integer.MAX_VALUE)) {
throw new ArithmeticException("Integer overflow");
}
return value;
}
public static int multAccum(int oldAcc, int newVal, int scale) {
final long res = intRangeCheck(
((long) oldAcc) + intRangeCheck((long) newVal * (long) scale)
);
return (int) res; // Safe downcast
}
    
}

/*
Komen :
Solusi compliant ini menunjukkan implementasi metode untuk memeriksa apakah nilai
jenis panjang jatuh dalam kisaran representable int menggunakan teknik upcasting.
Implementasi berbagai pemeriksaan untuk jenis integer primitif yang lebih kecil serupa.
dan pendekatan ini tidak dapat diterapkan pada nilai-nilai tipe lama karena lama
tipe integral primitif yang terbesar. Menggunakan teknik BigInteger sebaliknya ketika asli
variabel adalah dari jenis yang panjang.
*/
