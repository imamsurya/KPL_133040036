/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1n;

/**
 *
 * @author ACER
 */
public class WideSample {
    
public static int subFloatFromInt(int op1, float op2)
throws ArithmeticException {
// The significand can store at most 23 bits
if ((op2 > 0x007fffff) || (op2 < -0x800000)) {
throw new ArithmeticException("Insufficient precision");
}
return op1 - (int)op2;
}
public static void main(String[] args) {
int result = subFloatFromInt(1234567890, 1234567890);
System.out.println(result);
}
}

/*
Komen :
alam contoh noncompliant kode ini, dua identik besar integer literal dikirimkan sebagai
argumen ke subFloatFromInt() metode. Argumen kedua adalah dipaksa untuk mengapung,
melemparkan kembali ke int, dan dikurangi dari nilai tipe int. Hasilnya adalah kembali sebagai nilai
jenis int.
Metode ini bisa memiliki hasil yang tidak diharapkan karena kehilangan presisi. Di FPstrict
modus, nilai tipe Float memiliki 23 mantissa bit, sedikit tanda dan eksponen 8-bit.
*/
  

