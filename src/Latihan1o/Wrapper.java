/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1o;

import java.util.ArrayList;

/**
 *
 * @author ACER
 */
//public class Wrapper {
    
//public static void main(String[] args) {
//Integer i1 = 100;
//Integer i2 = 100;
//Integer i3 = 1000;
//Integer i4 = 1000;
//System.out.println(i1.equals(i2));
//System.out.println(!i1.equals(i2));
//System.out.println(i3.equals(i4));
//System.out.println(!i3.equals(i4));
//}
    
//}

public class Wrapper {
public static void main(String[] args) {
// Create an array list of integers
ArrayList<Integer> list1 = new ArrayList<Integer>();
for (int i = 0; i < 10; i++) {
list1.add(i + 1000);
}
// Create another array list of integers, where each element
// has the same value as the first one
ArrayList<Integer> list2 = new ArrayList<Integer>();
for (int i = 0; i < 10; i++) {
list2.add(i + 1000);
}
// Count matching values
int counter = 0;
for (int i = 0; i < 10; i++) {
if (list1.get(i).equals(list2.get(i))) { // Uses 'equals()'
counter++;
}
}
// Print the counter: 10 in this example
System.out.println(counter);
}
}

/*
Komen :
Java Collecton hanya mengandung objek; mereka tidak bisa mengandung tipe primitif. Lebih lanjut, jenis
parameter dari semua Jawa generik harus jenis objek daripada tipe primitif. Yaitu
mencoba untuk menyatakan ArrayList <int>(yang akan, agaknya, mengandung nilai-nilai tipe
Int) gagal pada waktu kompilasi karena tipe int tidak jenis objek. Deklarasi yang sesuai
akan ArrayList <Integer>, yang membuat penggunaan wrapper kelas dan autoboxing.
Contoh kode noncompliant ini mencoba untuk menghitung indeks dalam array
Daftar 1 dan list2 yang mempunyai nilai yang setara. Ingat bahwa kelas Integer diperlukan untuk memoize
hanya orang-orang nilai bilangan bulat di kisaran-128 ke 127; itu mungkin kembali sebuah objek nonunique untuk setiap
nilai di luar jangkauan itu. Akibatnya, ketika membandingkan autoboxed integer nilai di luar
Rentang tersebut, == operator mungkin kembali palsu dan contoh bisa menipu output 0.
*/
