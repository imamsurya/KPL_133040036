/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan1q;

/**
 *
 * @author ACER
 */
public class multAccum {
    
public static int multAccum(int oldAcc, int newVal, int scale) {
return Math.addExact(oldAcc, Math.multiplyExact(newVal, scale));
}
}

/*
Komen :
Salah satu operasi dalam contoh noncompliant kode ini dapat mengakibatkan suatu overflow occurs terjadi, 
hasilnya akan salah.
Solusi ini compliant menggunakan metode safeAdd() dan safeMultiply() didefinisikan dalam
Prasyarat pengujian bagian untuk melakukan operasi integral yang aman atau melempar aritmatika-
Pengecualian pada Overflow.
*/

