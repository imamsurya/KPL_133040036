/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author ACER
 */
class Helper {
public void handle(Socket socket) {
// ...
}
}
final class RequestHandler {
private final Helper helper = new Helper();
private final ServerSocket server;
private RequestHandler(int port) throws IOException {
server = new ServerSocket(port);
}
public static RequestHandler newInstance() throws IOException {
return new RequestHandler(0); // Selects next available port
}
public void handleRequest() {
new Thread(new Runnable() {
public void run() {
try {
helper.handle(server.accept());
} catch (IOException e) {
// Forward to handler
}
}
}).start();
}
} Co
        
/*
Modul 5 - latihan 2.1
Komen :
Contoh kode noncompliant ini menunjukkan pola desain thread setiap pesan.
Kelas RequestHandler menyediakan metode umum pabrik statis sehingga 
penelepon dapat memperoleh sebuah instance RequestHandler.
        */