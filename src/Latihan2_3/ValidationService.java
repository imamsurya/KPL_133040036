/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_3;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author ACER
 */
public final class ValidationService {
private final ExecutorService pool;
public ValidationService(int poolSize) {
pool = Executors.newFixedThreadPool(poolSize);
}
public void shutdown() {
pool.shutdown();
}
public StringBuilder fieldAggregator(String... inputs)
throws InterruptedException, ExecutionException {
StringBuilder sb = new StringBuilder();
// Stores the results
Future<String>[] results = new Future[inputs.length];
// Submits the tasks to thread pool
for (int i = 0; i < inputs.length; i++) {
results[i] = pool.submit(
new ValidateInput<String>(inputs[i], pool));
}
for (int i = 0; i < inputs.length; i++) { // Aggregates the results
sb.append(results[i].get());
}
return sb;
}
}
public final class ValidateInput<V> implements Callable<V> {
private final V input;
private final ExecutorService pool;
ValidateInput(V input, ExecutorService pool) {
this.input = input;
this.pool = pool;
}
@Override public V call() throws Exception {
// If validation fails, throw an exception here
// Subtask
Future<V> future = pool.submit(new SanitizeInput<V>(input));
return (V) future.get();
}
}

/*
Modul 5 - Latihan 2.3
komen :
Contoh kode noncompliant ini rentan terhadap kebuntuan thread-starvation. Terdiri dari ValidationService kelas, 
yang melakukan berbagai tugas validasi input memeriksa apakah 
sebuah field yang diberikan pengguna-ada di database back-end.
*/