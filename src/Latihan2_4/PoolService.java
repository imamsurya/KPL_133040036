/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_4;

/**
 *
 * @author ACER
 */
final class PoolService {
private final ExecutorService pool = Executors.newFixedThreadPool(10);
public void doSomething() {
pool.execute(new Task());
}
}
final class Task implements Runnable {
@Override public void run() {
// ...
throw new NullPointerException();
// ...
}
}

/*
Modul 5 - latihan 2.4
Komen :
ontoh kode noncompliant ini mengajukan kelas SocketReader sebagai tugas ke thread pool 
yang dideklarasikan dalam PoolService.
*/

/*final class PoolService {
// The values have been hard-coded for brevity
ExecutorService pool = new CustomThreadPoolExecutor(
10, 10, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(10));
// ...
}
class CustomThreadPoolExecutor extends ThreadPoolExecutor {
// ... Constructor ...
public CustomThreadPoolExecutor(
int corePoolSize, int maximumPoolSize, long keepAliveTime,
