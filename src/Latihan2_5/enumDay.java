/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2_5;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 * @author ACER
 */
public enum Day {
MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}
public final class Diary {
private static final ThreadLocal<Day> days =
new ThreadLocal<Day>() {
// Initialize to Monday
protected Day initialValue() {
return Day.MONDAY;
}
};
private static Day currentDay() {
return days.get();
}
public static void setDay(Day newDay) {
days.set(newDay);
}
// Performs some thread-specific task
public void threadSpecificTask() {
// Do task ...
}
}
public final class DiaryPool {
final int numOfThreads = 2; // Maximum number of threads allowed in pool
final Executor exec;
final Diary diary;
DiaryPool() {
exec = (Executor) Executors.newFixedThreadPool(numOfThreads);
diary = new Diary();
}
public void doSomething1() {
exec.execute(new Runnable() {
    @Override public void run() {
diary.setDay(Day.FRIDAY);
diary.threadSpecificTask();
}
});
}
public void doSomething2() {
exec.execute(new Runnable() {
@Override public void run() {
diary.threadSpecificTask();
}
});
}
public static void main(String[] args) {
DiaryPool dp = new DiaryPool();
dp.doSomething1(); // Thread 1, requires current day as Friday
dp.doSomething2(); // Thread 2, requires current day as Monday
dp.doSomething2(); // Thread 3, requires current day as Monday
}
}

/*
Modul 5 - latihan 2.5
Komen :
Contoh kode noncompliant ini terdiri dari dua kelas (diary dan DiaryPool) dan penghitungan day (day). 
Kelas Diary menggunakan variabel ThreadLocal untuk menyimpan informasi tertentu thread, seperti setiap tugas saat ini hari.
*/

