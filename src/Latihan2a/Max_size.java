/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2a;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 *
 * @author ACER
 */
public class Max_size {
    
public final int MAX_SIZE = 1024;
public String readBytes(Socket socket) throws IOException {
InputStream in = socket.getInputStream();
byte[] data = new byte[MAX_SIZE+1];
int offset = 0;
int bytesRead = 0;
while ((bytesRead = in.read(data, offset, data.length - offset)) != -1) {
offset += bytesRead;
if (offset >= data.length) {
throw new IOException("Too much input");
}
}
String str = new String(data, 0, offset, "UTF-8");
in.close();
return str;
}
}

/*
Komen :
Contoh kode noncompliant ini mencoba untuk membaca sampai 1024 bytes dari soket dan membangun
String dari data ini. Hal ini dilakukan dengan membaca dalam byte dalam beberapa saat loop, 
seperti yang direkomendasikan oleh aturan FIO10-J. Jika pernah mendeteksi bahwa soket 
memiliki lebih dari 1024 bytes tersedia, itu melempar
pengecualian. Hal ini untuk mencegah tidak terpercaya masukan dari berpotensi melelahkan memori program
Kode ini gagal untuk menjelaskan interaksi antara karakter diwakili dengan
multibyte encoding dan batas-batas antara sumber iterasi pengulangan.
*/
