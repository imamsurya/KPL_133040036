/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2c;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Fis {
    
FileInputStream fis = null;
try {
    try {
        fis = new FileInputStream("SomeFile");
    } catch (FileNotFoundException ex) {
        Logger.getLogger(Fis.class.getName()).log(Level.SEVERE, null, ex);
    }
DataInputStream dis = new DataInputStream(fis);
byte[] data = new byte[1024];
    try {
        dis.readFully(data);
    } catch (IOException ex) {
        Logger.getLogger(Fis.class.getName()).log(Level.SEVERE, null, ex);
    }
    try {
        String result = new String(data, "UTF-16LE");
    } catch (UnsupportedEncodingException ex) {
        Logger.getLogger(Fis.class.getName()).log(Level.SEVERE, null, ex);
    }
} catch (IOException x) static {
// Handle error
} finally {
if (fis != null) {
try {
fis.close();
} catch (IOException x) {
// Forward to handler
}
}
}
}

/*
Komen :
Solusi ini sesuai secara eksplisit menentukan pengkodean karakter yang dimaksudkan dalam kedua
argumen ke konstruktor String.

*/
