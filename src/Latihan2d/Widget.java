/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2d;

/**
 *
 * @author ACER
 */
public class Widget {
 private int total; // Declared private
public int getTotal () {
return total;
}
// Definitions for add() and remove() remain the same
   
}

/*
Komen :
Solusi ini sesuai menyatakan total sebagai pribadi dan menyediakan aksesor umum sehingga
diperlukan anggota dapat diakses di luar paket saat ini. Add() dan remove()
metode mengubah nilainya tanpa melanggar setiap kelas invariants.
Perhatikan bahwa perawatan harus diambil ketika memberikan referensi ke obyek-obyek pribadi bisa berubah
dari metode aksesor; Lihat aturan OBJ05-J untuk informasi lebih lanjut.
tu adalah praktik yang baik untuk menggunakan metode seperti add(), remove(), dan getTotal() untuk memanipulasi
keadaan internal pribadi. Metode ini dapat melakukan fungsi tambahan, seperti
masukan validasi dan keamanan manajer memeriksa.
*/