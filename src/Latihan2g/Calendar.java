/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2g;

import java.util.Date;

/**
 *
 * @author ACER
 */
public class Calendar {

    private boolean after(Object when) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int compareTo(Calendar calendar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   class CalendarSubclass extends Calendar {
public boolean after(Object when) {
// Correctly calls Calendar.compareTo()
if (when instanceof Calendar &&
super.compareTo((Calendar) when) == 0) {
return true;
}
return super.after(when);
}
public int compareTo(Calendar anotherCalendar) {
return compareDays(this.getFirstDayOfWeek(),
anotherCalendar.getFirstDayOfWeek());
}
private int compareDays(int currentFirstDayOfWeek,
int anotherFirstDayOfWeek) {
return (currentFirstDayOfWeek > anotherFirstDayOfWeek) ? 1
: (currentFirstDayOfWeek == anotherFirstDayOfWeek) ? 0 : -1;
}
public static void main(String[] args) {
CalendarSubclass cs1 = new CalendarSubclass();
cs1.setTime(new Date());
// Date of last Sunday (before now)
cs1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
// Wed Dec 31 19:00:00 EST 1969
CalendarSubclass cs2 = new CalendarSubclass();
// Expected to print true
System.out.println(cs1.after(cs2));
}
// Implementation of other Calendar abstract methods

        private void setTime(Date date) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}

/*
Komen :
Contoh kode noncompliant ini menimpa after() metode dan compareTo() dari
kelas java.util.Calendar. Calendar.after() metode mengembalikan nilai boolean yang
menunjukkan apakah atau tidak kalendar mewakili waktu setelah yang diwakili oleh yang ditentukan
Parameter obyek.
*/
