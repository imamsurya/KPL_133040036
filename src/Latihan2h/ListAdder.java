/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2h;

/**
 *
 * @author ACER
 */
public class ListAdder {
 private static <T> void addToList(List<T> list, T t) {
list.add(t); // No warning generated
}
private static <T> void printNum(T type) {
if (type instanceof Integer) {
List<Integer> list = new ArrayList<Integer>();
addToList(list, 42);
System.out.println(list.get(0));
}
else if (type instanceof Double) {
List<Double> list = new ArrayList<Double>();
addToList(list, 42.0); // Will not compile with 42 instead of 42.0
System.out.println(list.get(0));
} else {
System.out.println("Cannot print in the supplied type");
}
}
public static void main(String[] args) {
double d = 42;
int i = 42;
System.out.println(d);
ListAdder.printNum(d);
System.out.println(i);
ListAdder.printNum(i);
}
}   
/*
Komen : 
Contoh kode noncompliant ini mengkompilasi dan menjalankan bersih karena menekan
dicentang peringatan yang dihasilkan oleh metode List.add() mentah. Metode printOne()
bermaksud untuk mencetak nilai 1 sebagai int atau ganda tergantung pada jenis
jenis variabel.
*/

