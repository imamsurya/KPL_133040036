/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2i;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ACER
 */
public class ListModifierExample {
    public static void modify(List<List<String>> list) {
list.set( 1, Arrays.asList("forty-two")); // No warning
for (List<String> ls : list) {
for (String string : ls) { // ClassCastException on 42
System.out.println(string);
}
}
}
public static void main(String[] args) {
List<String> s = Arrays.asList("foo", "bar");
List<String> s2 = Arrays.asList("baz", "quux");
List<List<String>> list = new ArrayList<List<String>>();
list.add(s);
list.add(s2);
modify(list);
}
}

/*
Komen :
Di java sarray yang sering digunakan untuk mengirimkan data biner mentah serta karakter-dikodekan
data. Upaya untuk membaca data biner mentah seolah-olah data yang dikodekan karakter sering gagal karena
beberapa byte jatuh di luar default atau skema pengkodean tertentu dan untuk alasan itu
gagal untuk menunjukkan karakter yang valid
*/
