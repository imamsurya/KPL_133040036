/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2j;

import java.util.Date;

/**
 *
 * @author ACER
 */

    public final class MutableClass { // Copy constructor
private final Date date;
public MutableClass(MutableClass mc) {
this.date = new Date(mc.date.getTime());
}
public MutableClass(Date d) {
this.date = new Date(d.getTime()); // Make defensive copy
}
public Date getDate() {
return (Date) date.clone(); // Copy and return
}
}

/*
Komen :
Solusi ini compliant menggunakan constructor salinan yang menginisialisasi MutableClass instance
Ketika sebuah argumen dari jenis yang sama (atau subtipe) dilewatkan ke itu.
Pendekatan ini berguna ketika bidang contoh dinyatakan akhir. Penelepon meminta salinan
dengan menerapkan salinan konstruktor dengan instance MutableClass yang ada sebagai argumen
*/