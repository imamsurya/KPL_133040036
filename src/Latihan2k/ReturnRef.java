/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2k;

import java.util.Hashtable;

/**
 *
 * @author ACER
 */
public class ReturnRef {
    private Hashtable<Integer,String> getValues(){
return (Hashtable<Integer, String>) ht.clone(); // Shallow copy
}
public static void main(String[] args) {
ReturnRef rr = new ReturnRef();
// Prints nonsensitive data
Hashtable<Integer,String> ht1 = rr.getValues();
// Untrusted caller can only modify copy
ht1.remove(1);
// Prints nonsensitive data
Hashtable<Integer,String> ht2 = rr.getValues();
}
    
}

/*
Komen :
Dalam contoh noncompliant kode ini, kelas ReturnRef berisi sebuah instance Hashtable pribadi
bidang. Tabel hash menyimpan abadi tapi sensitif data (misalnya, nomor jaminan sosial
[SSNs]). Metode getValues() memberi akses pemanggil ke Tabel hash dengan kembali
referensi untuk itu. Pemanggil tidak terpercaya dapat menggunakan metode ini untuk mendapatkan akses ke Tabel hash;
sebagai akibatnya, entri Tabel hash dapat menjadi jahat ditambahkan, dihapus atau diganti. Selain itu,
beberapa benang dapat melakukan modifikasi ini, menyediakan kesempatan yang luas untuk kondisi.
*/