/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2l;

import java.net.HttpCookie;

/**
 *
 * @author ACER
 */
public class MutableDemo {
    // java.net.HttpCookie is mutable
public void useMutableInput(HttpCookie cookie) {
if (cookie == null) {
throw new NullPointerException();
}
// Create copy
cookie = (HttpCookie)cookie.clone();
// Check whether cookie has expired
if (cookie.hasExpired()) {
// Cookie is no longer valid; handle condition by throwing an exception
}
doLogic(cookie);
}

    private void doLogic(HttpCookie cookie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/* 
Komen :
Contoh kode noncompliant ini berisi TOCTOU kerentanan. Karena cookie
bisa berubah masukan, penyerang dapat menyebabkan ia berakhir antara cek awal (
hasExpired() panggilan) dan sebenarnya menggunakan (panggilan doLogic()).
Solusi ini  menghindari kerentanan TOCTOU dengan menyalin masukan bisa berubah
dan melakukan semua operasi pada salinan. Akibatnya, diangkat penyerang perubahan untuk bisa berubah
masukan tidak mempengaruhi salinan.
*/