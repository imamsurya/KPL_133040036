/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2m;

/**
 *
 * @author ACER
 */
public class Coordinates {
    private int x;
private int y;
private class Point {
private void getPoint() {
System.out.println("(" + x + "," + y + ")");
}
}
}
class AnotherClass {
public static void main(String[] args) {
Coordinates c = new Coordinates();
Coordinates.Point p = c.new Point(); // Fails to compile
p.getPoint();
}
}

/*
KOmen :
Contoh kode noncompliant ini memperlihatkan koordinat pribadi (x, y) melalui
metode getPoint() kelas batin. Akibatnya, golongan AnotherClass yang dimiliki
untuk paket yang sama juga dapat mengakses koordinat.
Gunakan specifier akses pribadi untuk menyembunyikan batin kelas dan metode semua unit dan
konstruktor
*/