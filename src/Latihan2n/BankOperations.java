/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan2n;

/**
 *
 * @author ACER
 */
public class BankOperations {
private volatile boolean initialized = false;
public BankOperations() {
if (!performSSNVerification()) {
return; // object construction failed
}
this.initialized = true; // Object construction successful
}
private boolean performSSNVerification() {
return false;
}
public void greet() {
if (!this.initialized) {
throw new SecurityException("Invalid SSN!");
}
System.out.println(
"Welcome user! You may now use all the features.");
}    
}

/*
Komen :
Contoh kode noncompliant ini, berdasarkan contoh oleh Kabutz [Kabutz 2001], mendefinisikan
Pembina BankOperations kelas sehingga melakukan verifikasi menggunakan SSN
performSSNVerification() metode.
*/