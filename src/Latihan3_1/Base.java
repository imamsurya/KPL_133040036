/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3_1;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
class Base {
private final Object lock = new Object();
public void doSomething() {
synchronized (lock) {
// ...
}
}
}
class Derived extends Base {
Logger logger = // Initialize
private final Object lock = new Object();
@Override public void doSomething() {
synchronized (lock) {
try {
super.doSomething();
} finally {
logger.log(Level.FINE, "Did something");
}
}
}
}

/*
Modul 5 - latihan 3.1
Komen :
Contoh kode noncompliant ini mendefinisikan sebuah metode doSomething() 
di kelas dasar yang menggunakan kunci akhir pribadi sesuai dengan aturan LCK00-J.
Mungkin untuk beberapa threads menyebabkan entri akan didata dalam urutan yang berbeda dari urutan di mana 
tugas yang dilakukan. Akibatnya, metode doSomething() kelas berasal tidak dapat digunakan dengan aman oleh
beberapa threads karena threads tidak aman .
*/