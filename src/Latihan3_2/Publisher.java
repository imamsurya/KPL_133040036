/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3_2;

/**
 *
 * @author ACER
 */
final class Publisher {
final int num;
private Publisher(int number) {
// Initialization
this.num = number;
}
public static Publisher newInstance(int number) {
Publisher published = new Publisher(number);
return published;
}
}

/*
Modul 5 - Latihan 3.2
Komen :
Contoh kode noncompliant ini menerbitkan ini referensi sebelum inisialisasi telah menyimpulkan dengan
menyimpan bidang umum kelas volatile statis. Akibatnya, 
threads lain dapat memperoleh sebuah instance penerbit sebagian initialized.
Jika inisialisasi objek (dan akibatnya, konstruksi) tergantung pada Cek keamanan dalam konstruktor, 
pemeriksaan keamanan dapat dilewati ketika pemanggil tidak terpercaya memperoleh contoh sebagian initialized. 
Untuk selengkapnya, lihat aturan OBJ11-J.
*/