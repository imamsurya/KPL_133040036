/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3_3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ACER
 */
public final class ConnectionFactory {
private static Connection dbConnection;
// Other fields ...
static {
Thread dbInitializerThread = new Thread(new Runnable() {
@Override public void run() {
// Initialize the database connection
try {
dbConnection = DriverManager.getConnection("connection string");
} catch (SQLException e) {
dbConnection = null;
}
}
});
// Other initialization, for example, start other threads
dbInitializerThread.start();
try {
dbInitializerThread.join();
} catch (InterruptedException ie) {
throw new AssertionError(ie);
}
}
public static Connection getConnection() {
if (dbConnection == null) {
throw new IllegalStateException("Error initializing connection");
}
return dbConnection;
}
public static void main(String[] args) {
// ...
Connection connection = getConnection();
}
}

/*
Modul 5 - Latihan 3.3
Komen :
Dalam contoh noncompliant kode ini, initializer statis mulai thread latar belakang sebagai bagian dari kelas inisialisasi. 
threads latar belakang mencoba untuk menginisialisasi koneksi databae tetapi harus menunggu sampai
semua anggota kelas ConnectionFactory, termasuk dbConnection, diinisialisasi.
*/

/*public final class ConnectionFactory {
private static Connection dbConnection;
// Other fields ...
static {
Thread dbInitializerThread = new Thread(new Runnable() {
@Override public void run() {
// Initialize the database connection
try {
dbConnection = DriverManager.getConnection("connection string");
} catch (SQLException e) {
dbConnection = null;
}
}
});
// Other initialization, for example, start other threads
dbInitializerThread.start();
try {
dbInitializerThread.join();
} catch (InterruptedException ie) {
throw new AssertionError(ie);
}
}
public static Connection getConnection() {
if (dbConnection == null) {
throw new IllegalStateException("Error initializing connection");
}
return dbConnection;
}
public static void main(String[] args) {
// ...
Connection connection = getConnection();
}
}
*/