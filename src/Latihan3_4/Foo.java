/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3_4;

/**
 *
 * @author ACER
 */
class Foo {
private volatile Helper helper;
public Helper getHelper() {
return helper;
}
public void initialize() {
helper = new Helper(42);
}
}
// Mutable but thread-safe Helper
public class Helper {
private volatile int n;
private final Object lock = new Object();
public Helper(int n) {
this.n = n;
}
public void setN(int value) {
synchronized (lock) {
n = value;
}
}
}

/*
Modul 5 - latihan 3.4
Komen :
Solusi ini compliant memerlukan helper yang dinyatakan volatile dan kelas Helperis immutable. 
Jika bidang helper tidak stabil, itu akan melanggar aturan VNA01-J. 
Menyediakan public static factory method  yang mengembalikan sebuah instance baru dari helper baik diizinkan dan didorong. 
Pendekatan ini memungkinkan contoh helper yang akan dibuat di konstruktor aprivate.
*/
