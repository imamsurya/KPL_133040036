/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3a;

/**
 *
 * @author ACER
 */
public class Mystate {
    private Object myState = null;
// Sets some internal state in the library
void setState(Object state) {
if (state == null) {
// Handle null state
}
// Defensive copy here when state is mutable
if (isInvalidState(state)) {
// Handle invalid state
}
myState = state;
}
// Performs some action using the state passed earlier
void useState() {
if (myState == null) {
// Handle no state (e.g., null) condition
}
// ...
}

    private boolean isInvalidState(Object state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/*
Komen :
Dalam contoh noncompliant kode ini, setState() dan useState() gagal untuk memvalidasi argumen mereka.
Pemanggil jahat bisa melewati sebuah negara yang tidak sah ke Perpustakaan, akibatnya merusak
Perpustakaan dan mengekspos kerentanan
Kerentanan tersebut sangat parah ketika keadaan internal berisi atau merujuk kepada
data sensitif atau sistem-kritis
*/