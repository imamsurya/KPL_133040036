/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3b;

import java.util.Random;
import java.util.Vector;

/**
 *
 * @author ACER
 */
public final class Container implements Runnable {
private final Vector<Integer> vector = new Vector<Integer>(1000);
private volatile boolean done = false;
public Vector<Integer> getVector() {
return vector;
}
public void shutdown() {
done = true;
}
@Override public synchronized void run() {
Random number = new Random(123L);
int i = vector.capacity();
while (!done && i > 0) {
vector.add(number.nextInt(100));
i--;
}
}
public static void main(String[] args) throws InterruptedException {
Container container = new Container();
Thread thread = new Thread(container);
thread.start();
Thread.sleep(5000);
container.shutdown();
}
}

/*
Komen :
Dalam kode noncompliant ini contoh, jika salah satu thread berulang kali memanggil metode assignValue()
dan thread lain berulang kali memanggil metode printLong(), metode printLong() bisa
kadang-kadang mencetak nilai saya yang bukan nol atau nilai j argumen.
Penting untuk memastikan bahwa argumen ke assignValue() metode yang diperoleh
dari bergejolak variabel atau diperoleh sebagai hasil dari atom membaca. Jika tidak, membaca
argumen variabel itu sendiri dapat mengekspos kerentanan.
Semantik dari volatile secara tegas mengecualikan jaminan atomicity senyawa
operasi yang melibatkan urutan memodifikasi baca-tulis seperti incrementing nilai.
Lihat aturan VNA02-J untuk informasi lebih lanjut.
*/