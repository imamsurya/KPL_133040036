/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3c;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author ACER
 */
public final class NetworkHandler {
private final ExecutorService executor;
NetworkHandler(int poolSize) {
this.executor = Executors.newFixedThreadPool(poolSize);
}
}
/*public void startThreads() {
for (int i = 0; i < 3; i++) {
executor.execute(new HandleRequest());
}
}
public void shutdownPool() {
executor.shutdown();
}
public static void main(String[] args) {
NetworkHandler nh = new NetworkHandler(3);
nh.startThreads();
nh.shutdownPool();
}
}
*/

/*
Komen : 
Contoh kode noncompliant ini berisi kelas NetworkHandler yang mempertahankan controller
benang. Kain controller mendelegasikan setiap permintaan baru thread pekerja. Untuk
menunjukkan kondisi ras dalam contoh ini, controller benang menyajikan tiga permintaan
dengan memulai thread tiga berturut-turut dari metode run(). Semua Thread yang didefinisikan untuk
milik kepala kelompok benang.
Implementasi ini berisi kerentanan kali-dari-cek, waktu penggunaan (TOCTOU)
karena memperoleh menghitung dan menyesuaikan daftar tanpa memastikan atomicity. Jika satu atau
permintaan baru lain yang terjadi setelah panggilan untuk activeCount() dan sebelum panggilan untuk
enumerate() dalam main() metode, jumlah thread dalam grup akan
peningkatan, tapi daftar disebutkan ta akan berisi hanya nomor awal, yaitu dua
Thread referensi: utama dan controller. Akibatnya, program akan gagal untuk akun
untuk baru dimulai benang di kepala grup thread
*/