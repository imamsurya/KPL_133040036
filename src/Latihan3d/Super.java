/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3d;

/**
 *
 * @author ACER
 */
public class Super {
   protected void doLogic() {
System.out.println("Super invoked");
}
}
//public class Sub extends Super {
//public void doLogic() {
//System.out.println("Sub invoked");
// Do sensitive operations
//} 
//}

/*
Komen :
Contoh kode noncompliant ini berisi TOCTOU kerentanan. Karena cookie
bisa berubah masukan, penyerang dapat menyebabkan ia berakhir antara cek awal (
hasExpired() panggilan) dan sebenarnya menggunakan (panggilan doLogic()).
Solusi ini compliant menghindari kerentanan TOCTOU dengan menyalin input bisa berubah
dan melakukan semua operasi pada copy. Akibatnya, seorang penyerang perubahan untuk bisa berubah
masukan tidak mempengaruhi salinan.
*/