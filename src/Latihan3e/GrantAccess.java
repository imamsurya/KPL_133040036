/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3e;

/**
 *
 * @author ACER
 */
public class GrantAccess {
    public void displayAccountStatus() {
System.out.print("Account details for admin: XX");
}
}
class GrantUserAccess extends GrantAccess {
@Override
public void displayAccountStatus() {
System.out.print("Account details for user: XX");
}
}
/*public class StatMethod {
public static void choose(String username) {
GrantAccess admin = new GrantAccess();
GrantAccess user = new GrantUserAccess();
}
if (username.equals("admin") {
admin.displayAccountStatus();
    return null;
} else {
user.displayAccountStatus();
}
}
public static void main(String[] args) {
choose("user");
}
}
*/

/*
Komen :
Dalam contoh ini noncompliant, programmer menyembunyikan metode statis daripada override
itu. Akibatnya, kode memanggil metode displayAccountStatus()
superclass di dua situs panggilan berbeda daripada menerapkan metode superclass di satu panggilan
situs dan metode subclass lainnya.
Dalam solusi ini compliant, programmer menyatakan metode displayAccountStatus()
sebagai contoh metode dengan menghapus kata kunci statis. Akibatnya, dinamis pengiriman
di situs panggilan menghasilkan hasil yang diharapkan. Anotasi @Override menunjukkan
disengaja override parent method.
*/