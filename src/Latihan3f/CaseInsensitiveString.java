/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3f;

/**
 *
 * @author ACER
 */
public final class CaseInsensitiveString {
private String s;
public CaseInsensitiveString(String s) {
if (s == null) {
throw new NullPointerException();
}
this.s = s;
}
public boolean equals(Object o) {
return o instanceof CaseInsensitiveString &&
((CaseInsensitiveString)o).s.equalsIgnoreCase(s);
}
public int hashCode() {return 0;
/* ... */}
public static void main(String[] args) {
CaseInsensitiveString cis = new CaseInsensitiveString("Java");
String s = "java";
System.out.println(cis.equals(s)); // Returns false now
System.out.println(s.equals(cis)); // Returns false now
}
}
 /*
Komen :
Contoh kode noncompliant ini mendefinisikan kelas CaseInsensitiveString yang mencakup
String dan menimpa equals() metode. Kelas CaseInsensitiveString tahu
tentang string biasa, tetapi String kelas memiliki pengetahuan tidak case-sensitive string.
Akibatnya, metode CaseInsensitiveString.equals() tidak harus berusaha untuk berinterpolasi
dengan objek kelas String.
Dalam larutan ini compliant, CaseInsensitiveString.equals() metode sederhana untuk
beroperasi hanya pada contoh-contoh kelas CaseInsensitiveString, akibatnya melestarikan
simetri.
*/
    

