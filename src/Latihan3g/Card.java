/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3g;

/**
 *
 * @author ACER
 */
public class Card {
    private final int number;
public Card(int number) {
this.number = number;
}
public boolean equals(Object o) {
if (!(o instanceof Card)) {
return false;
}
Card c = (Card)o;
return c.number == number;
}
public int hashCode() {return 0;
/* ... */}
}
class XCard extends Card {
private String type;
public XCard(int number, String type) {
super(number);
this.type = type;
}
public boolean equals(Object o) {
if (!(o instanceof Card)) {
return false;
}
// Normal Card, do not compare type
if (!(o instanceof XCard)) {
return o.equals(this);
}
// It is an XCard, compare type as well
XCard xc = (XCard)o;
return super.equals(o) && xc.type == type;
}
public int hashCode() {return 0;
/* ... */}
public static void main(String[] args) {
XCard p1 = new XCard(1, "type1");
Card p2 = new Card(1);
XCard p3 = new XCard(1, "type2");
System.out.println(p1.equals(p2)); // Returns true
System.out.println(p2.equals(p3)); // Returns true
System.out.println(p1.equals(p3)); // Returns false
// violating transitivity
}
}

/*
Komen :
Dalam contoh kode noncompliant, p1 dan p2 membandingkan sama dan p2 dan p3 membandingkan
sama, tapi p1 dan p3 Bandingkan tidak seimbang, melanggar persyaratan transitif. Masalah
adalah bahwa kelas kartu tidak memiliki informasi mengenai kelas XCard dan akibatnya tidak dapat menentukan
bahwa p2 dan p3 memiliki nilai yang berbeda untuk tipe field
Sayangnya, dalam hal ini tidak mungkin untuk memperpanjang kelas instantiable (sebagai lawan untuk
kelas abstrak) dengan menambahkan nilai atau bidang di subclass sambil menjaga equals()
kontrak. Menggunakan komposisi daripada warisan untuk mencapai efek yang diinginkan [Bloch
2008]. solusi compliant ini mengadopsi pendekatan ini dengan menambahkan sebuah field kartu pribadi untuk
XCard kelas dan menyediakan metode umum viewCard().
*/