/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3h;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author ACER
 */
public class Filter {
    public static void main(String[] args) throws MalformedURLException {
final URL allowed = new URL("http://mailwebsite.com");
if (!allowed.toString().equals(new URL(args[0]).toString())) {
throw new SecurityException("Access Denied");
}
// Else proceed
}
}

/*
Komen :
Solusi ini compliant membandingkan dua URL representasi string, sehingga menghindari
perangkap URL.equals().
Solusi ini masih memiliki masalah. Dua URL dengan representasi string yang berbeda masih bisa
merujuk ke sumber daya yang sama. Namun, solusi yang gagal aman dalam kasus ini karena
equals() kontrak yang diawetkan, dan sistem akan tidak memungkinkan sebuah URL yang jahat untuk menjadi
diterima oleh kesalahan.
*/