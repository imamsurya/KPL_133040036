/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3i;

import java.awt.RenderingHints.Key;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.Arrays;

/**
 *
 * @author ACER
 */
public class keysEqual {
    private static boolean keysEqual(Key key1, Key key2) {
if (key1.equals(key2)) {
return true;
}
    }
/*if (Arrays.equals(key1.getEncoded(), key2.getEncoded())) {
return true;
}
// More code for different types of keys here.
// For example, the following code can check if
// an RSAPrivateKey and an RSAPrivateCrtKey are equal:
if ((key1 instanceof RSAPrivateKey) &&
(key2 instanceof RSAPrivateKey)) {
if ((((RSAKey)key1).getModulus().equals(
((RSAKey)key2).getModulus())) &&
(((RSAPrivateKey) key1).getPrivateExponent().equals(
((RSAPrivateKey) key2).getPrivateExponent()))) {
return true;
}
}
return false;
    
}
}

/*
    Komen :
    Java.lang.Object.equals() metode secara default tidak dapat membandingkan komposit
benda-benda seperti kunci kriptografis. Hampir seluruh kelas berisi kunci kekurangan penerapan equals() yang
akan menimpa objek default implementasi. Dalam kasus tersebut, komponen
komposit objek harus dibandingkan secara individual untuk memastikan kebenaran.
Contoh kode noncompliant ini membandingkan dua kunci menggunakan metode equals().
Perbandingan dapat kembali palsu bahkan ketika contoh-contoh kunci mewakili sama
Logis kunci.
    Solusi ini compliant menggunakan metode equals() sebagai ujian pertama dan kemudian membandingkan
dikodekan versi tombol untuk memfasilitasi perilaku penyedia-independen. Sebagai contoh, ini
Kode dapat menentukan apakah RSAPrivateKey dan RSAPrivateCrtKey mewakili setara
kunci pribadi [Sun 2006].
    */