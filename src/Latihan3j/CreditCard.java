/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3j;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ACER
 */
public class CreditCard {
   // public final class CreditCard {
private final int number;
public CreditCard(int number) {
this.number = number;
}
public boolean equals(Object o) {
if (o == this) {
return true;
}
if (!(o instanceof CreditCard)) {
return false;
}
CreditCard cc = (CreditCard)o;
return cc.number == number;
}
public int hashCode() {
int result = 17;
result = 31 * result + number;
return result;
}
public static void main(String[] args) {
Map<CreditCard, String> m = new HashMap<CreditCard, String>();
m.put(new CreditCard(100), "4111111111111111");
System.out.println(m.get(new CreditCard(100)));
}
}

/*
Contoh kode noncompliant ini mengaitkan nomor kartu kredit dengan string yang menggunakan HashMap
dan kemudian mencoba untuk mengambil nilai string yang terkait dengan nomor kartu kredit.
Diharapkan nilai diperoleh adalah 4111111111111111; Diakses pada nilai sebenarnya null.
Solusi compliant ini menimpa hashCode() metode sehingga menghasilkan sama
nilai untuk setiap dua contoh yang dianggap sama dengan metode equals(). Bloch
membahas resep untuk menghasilkan seperti sebuah fungsi hash secara rinci [Bloch 2008].
*/
