/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3k;

/**
 *
 * @author ACER
 */
public class GameEntry {
    public enum Roshambo {ROCK, PAPER, SCISSORS}
private Roshambo value;
public GameEntry(Roshambo value) {
this.value = value;
}
public int beats(Object that) {
if (!(that instanceof GameEntry)) {
throw new ClassCastException();
}
GameEntry t = (GameEntry) that;
return (value == t.value) ? 0
: (value == Roshambo.ROCK && t.value == Roshambo.PAPER) ? -1
: (value == Roshambo.PAPER && t.value == Roshambo.SCISSORS) ? -1
: (value == Roshambo.SCISSORS && t.value == Roshambo.ROCK) ? -1
: 1;
}
}

/*
Komen :
Program ini mengimplementasikan klasik permainan batu-kertas-gunting, menggunakan compareTo()
operator untuk menentukan pemenang permainan
Namun, permainan ini melanggar properti transitif diperlukan karena batu mengalahkan
gunting, dan gunting mengalahkan kertas, tetapi batu tidak memukul kertas.
*/