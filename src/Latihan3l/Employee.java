/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3l;

/**
 *
 * @author ACER
 */
public class Employee {
    private String name;
private double salary;
Employee(String empName, double empSalary) {
this.name = empName;
this.salary = empSalary;
}
public void setEmployeeName(String empName) {
this.name = empName;
}
public void setSalary(double empSalary) {
this.salary = empSalary;
}
@Override
public boolean equals(Object o) {
if (!(o instanceof Employee)) {
return false;
}
Employee emp = (Employee)o;
return emp.name.equals(name);
}
public int hashCode() {return 0;
/* ... */}
}
// Client code
//Map<Employee, Calendar> map = 

//}

/*
Komen :
Contoh kode noncompliant ini mendefinisikan kelas bisa berubah karyawan yang terdiri dari
kolom nama dan gaji, nilai-nilai yang dapat diubah dengan menggunakan setEmployeeName() dan
metode setSalary(). Metode equals() yang akan ditimpa untuk menyediakan fasilitas perbandingan
oleh nama karyawan.
Solusi ini sesuai untuk menambah employeeID akhir field yang berubah setelah inisialisasi.
Metode equals() membandingkan objek karyawan berdasarkan bidang ini
*/