/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3n;

import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class MyFrame {
    private JFrame frame;
private byte[] buffer = new byte[16 * 1024 * 1024]; // Now decoupled
}

/*
Komentar :
Pertimbangkan contoh dari 1,5 JDK dan sebelumnya. Contoh noncompliant kode berikut
mengalokasikan 16MB buffer digunakan untuk kembali sebuah objek JFrame ayunan. Meskipun kekurangan api JFrame
metode finalize(), JFrame meluas AWT. Bingkai, yang memiliki finalize() metode.
Ketika sebuah objek di Google menjadi terjangkau, pengumpul sampah tidak merebut kembali
penyimpanan untuk byte buffer karena kode dalam metode mewarisi finalize() mungkin menyebutnya.
Akibatnya, byte buffer harus bertahan setidaknya sampai finalize() warisan metode
untuk kelas di Google selesai pelaksanaannya dan tidak boleh direklamasi sampai berikut
siklus pengumpulan sampah.
Ketika superclass mendefinisikan sebuah metode finalize(), pastikan untuk decouple objek yang dapat
akan segera mengumpulkan sampah dari orang-orang yang harus bergantung pada finalizer. Ini sesuai
solusi memastikan bahwa buffer dapat direklamasi segera menjadi objek
terjangkau.
*/


