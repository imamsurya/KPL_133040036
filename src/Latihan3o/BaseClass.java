/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3o;

import java.util.Date;

/**
 *
 * @author ACER
 */
public class BaseClass {
    protected void finalize() throws Throwable {
System.out.println("Superclass finalize!");
doLogic();
}
public void doLogic() throws Throwable {
System.out.println("This is super-class!");
}
}
class SubClass extends BaseClass {
private Date d; // Mutable instance field
protected SubClass() {
d = new Date();
}
protected void finalize() throws Throwable {
System.out.println("Subclass finalize!");
try {
// Cleanup resources
d = null;
} finally {
super.finalize(); // Call BaseClass's finalizer
}
}
public void doLogic() throws Throwable {
// Any resource allocations made here will persist
// Inconsistent object state
System.out.println(
"This is sub-class! The date object is: " + d);
// 'd' is already null
}
}
/*public class BadUse {
public static void main(String[] args) {
try {
BaseClass bc = new SubClass();
// Artificially simulate finalization (do not do this)
System.runFinalizersOnExit(true);
} catch (Throwable t) {
// Handle error
}
}
}
*/

/*
Komen :
Kelas SubClass mengabaikan dilindungi finalize() metode dan melakukan pembersihan
kegiatan. Selanjutnya, itu panggilan super.finalize() untuk memastikan bahwa superclass yang juga selesai.
BaseClass tidak curiga memanggil metode doLogic(), yang terjadi untuk ditimpa
di SubClass. Ini menghidupkan referensi untuk SubClass yang tidak hanya mencegah
dari menjadi sampah yang dikumpulkan tetapi juga mencegah dari memanggil yang finalizer untuk menutup baru
sumber daya yang mungkin telah dialokasikan dengan metode yang disebut.
Joshua Bloch [Bloch 2008] menunjukkan menerapkan metode stop() secara eksplisit sedemikian rupa sehingga
Ia meninggalkan kelas dalam keadaan yang tidak dapat ini digunakan dalan di luar seumur hidup. Bidang dia dalam kelas
dapat Ligt kelas Apakah tidak dapat ini digunakan dalan. Rukan metode kelas harus Periksa sebelum bidang ini
untuk beroperasi pada kelas. Ini adalah sama dengan "diinisialisasi bendera"-compliant solusi dibahas
dalam peraturan OBJ11-J. Seperti biasa, tempat yang baik untuk memanggil penghentian logika 
akhirnya blok.
*/