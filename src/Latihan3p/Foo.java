/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3p;

/**
 *
 * @author ACER
 */
class Foo implements Runnable {
public void run() {
try {
Thread.sleep(1000);
} catch (InterruptedException e) {
Thread.currentThread().interrupt(); // Reset interrupted status
}
}
}

/*
Komen :
Sebuah solusi yang lebih mahal adalah untuk menyatakan kelas anonim sehingga finalize()
metode adalah guaran Tee untuk menjalankan untuk superclass. Solusi ini dapat diterapkan untuk umum nonfinal
kelas. "Finalizer wali objek pasukan super.finalize disebut jika sebuah subclass
mengabaikan finalize() dan tidak secara eksplisit panggilan super.finalize"[JLS 2005].
Masalahnya Pemesanan dapat berbahaya ketika berhadapan dengan kode asli. Misalnya, jika
referensi A objek objek B (baik secara langsung maupun reflektif) dan mendapat kedua rampung pertama,
A finalizer mungkin berakhir dereferencing menggantung asli pointer
*/