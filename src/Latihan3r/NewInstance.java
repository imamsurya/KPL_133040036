/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3r;

/**
 *
 * @author ACER
 */
public class NewInstance {
    private static Throwable throwable;
private NewInstance() throws Throwable {
throw throwable;
}
public static synchronized void undeclaredThrow(Throwable throwable) {
// These exceptions should not be passed
if (throwable instanceof IllegalAccessException ||
throwable instanceof InstantiationException) {
// Unchecked, no declaration required
throw new IllegalArgumentException();
}
NewInstance.throwable = throwable;
try {
// Next line throws the Throwable argument passed in above,
// even though the throws clause of class.newInstance fails
// to declare that this may happen
NewInstance.class.newInstance();
} catch (InstantiationException e) { /* Unreachable */
} catch (IllegalAccessException e) { /* Unreachable */
} finally { // Avoid memory leak
NewInstance.throwable = null;
}
}
}
//public class UndeclaredException {
//public static void main(String[] args) {
// No declared checked exceptions
//NewInstance.undeclaredThrow(
//new Exception("Any checked exception"));
//}
//Class.netInstance Workarounds
// public static void main(String[] args) {
// try {
// NewInstance.undeclaredThrow(
// new IOException("Any checked exception"));
// } catch (Throwable e) {
// if (e instanceof IOException) {
// System.out.println("IOException occurred");
// } else if (e instanceof RuntimeException) {
// throw (RuntimeException) e;
// } else {
// // Forward to handler
// }
// }
//}

/*
Komen :
Contoh kode noncompliant ini melempar dideklarasikan pengecualian diperiksa. UndeclaredThrow()
metode mengambil argumen Throwable, dan memanggil fungsi yang akan melemparkan
argumen tanpa menyatakan itu. Sementara undeclaredThrow() menangkap pengecualian
fungsi menyatakan bahwa itu mungkin melemparkan, itu meskipun demikian melempar argumen itu diberikan tanpa
mengenai apakah argumen adalah salah satu pengecualian menyatakan.
Solusi ini compliant menggunakan java.lang.reflect.Constructor.newInstance() agak
daripada Class.newInstance(). Metode Constructor.newInstance() membungkus pengecualian
dibuang dari dalam konstruktor pengecualian diperiksa disebut
InvocationTargetException.
*/