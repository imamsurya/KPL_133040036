/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3s;

/**
 *
 * @author ACER
 */
public class Capitalized {
    boolean isCapitalized(String s) {
if (s == null) {
throw new NullPointerException();
}
if (s.equals("")) {
return true;
}
String first = s.substring(0, 1);
String rest = s.substring(1);
return (first.equals(first.toUpperCase()) &&
rest.equals(rest.toLowerCase()));
}
}

/*
Komen :
Metode isCapitalized() dalam contoh kode noncompliant ini menerima string dan
mengembalikan nilai true ketika string yang terdiri dari huruf kapital diikuti huruf. The
metode ini juga melempar RuntimeException ketika melewati sebuah argumen null string.
Solusi ini compliant melempar NullPointerException untuk menunjukkan spesifik istimewa
kondisi.
*/
