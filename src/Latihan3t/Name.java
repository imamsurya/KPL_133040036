/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3t;

/**
 *
 * @author ACER
 */
public class Name {
    boolean isName(String s) {
if (s == null) {
return false;
}
String names[] = s.split(" ");
if (names.length != 2) {
return false;
}
return (isCapitalized(names[0]) && isCapitalized(names[1]));
}

    private boolean isCapitalized(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/*
Komen :
Contoh kode noncompliant ini mendefinisikan metode isName() yang mengambil String
argumen dan kembali benar jika string tertentu adalah nama yang valid. Nama yang valid didefinisikan sebagai
dua huruf besar kata-kata yang dipisahkan oleh satu atau lebih ruang. Alih-alih memeriksa untuk melihat
Apakah string tertentu null, metode menangkap NullPointerException dan kembali
palsu.
Solusi ini sesuai secara eksplisit cek argumen String untuk null daripada penangkapan
NullPointerException.
*/