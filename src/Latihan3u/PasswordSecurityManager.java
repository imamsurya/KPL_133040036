/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan3u;

/**
 *
 * @author ACER
 */
class PasswordSecurityManager extends SecurityManager {
private boolean isExitAllowedFlag;
public PasswordSecurityManager(){
super();
isExitAllowedFlag = false;
}
public boolean isExitAllowed(){
return isExitAllowedFlag;
}
@Override
public void checkExit(int status) {
if (!isExitAllowed()) {
throw new SecurityException();
}
super.checkExit(status);
}
public void setExitAllowed(boolean f) {
isExitAllowedFlag = f;
}
}
/*public class InterceptExit {
public static void main(String[] args) {
PasswordSecurityManager secManager =
new PasswordSecurityManager();
System.setSecurityManager(secManager);
try {
// ...
System.exit(1); // Abrupt exit call
} catch (Throwable x) {
if (x instanceof SecurityException) {
System.out.println("Intercepted System.exit()");
// Log exception
} else {
// Forward to exception handler
}
}
// ...
secManager.setExitAllowed(true); // Permit exit
// System.exit() will work subsequently
// ...
}
}
*/

/*
Komen :
Solusi ini compliant menginstall manajer keamanan kustom PasswordSecurityManager
yang menimpa checkExit() metode yang didefinisikan di kelas SecurityManager. Ini menimpa
diperlukan untuk mengaktifkan doa pembersihan kode sebelum mengizinkan pengeluaran. Default
checkExit() metode dalam kelas SecurityManager tidak memiliki fasilitas ini.
Implementasi ini menggunakan sebuah bendera internal untuk melacak Apakah diperbolehkan keluar. The
metode setExitAllowed() menetapkan tanda ini. Metode checkExit() melempar SecurityException
Ketika bendera unset (yaitu, false). Karena bendera ini tidak awalnya diatur, normal
pengecualian pengolahan bypasses panggilan awal untuk System.exit(). Program menangkap
SecurityException dan melakukan operasi pembersihan wajib, termasuk penebangan
pengecualian. Metode System.exit() diaktifkan hanya setelah pembersihan selesai.
*/