/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4A;

/**
 *
 * @author ACER
 */
import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
class Login {
public Connection getConnection() throws SQLException {
DriverManager.registerDriver(new
com.microsoft.sqlserver.jdbc.SQLServerDriver());
String dbConnection;
    dbConnection = (String) PropertyManager.getProperty("db.connection");
// Can hold some value like
// "jdbc:microsoft:sqlserver://<HOST>:1433,<UID>,<PWD>"
return DriverManager.getConnection(dbConnection);
}
String hashPassword(char[] password) {
// Create hash of password
}
public void doPrivilegedAction(String username, char[] password)
throws SQLException {
Connection connection = getConnection();
if (connection == null) {
// Handle error
}
try {
String pwd = hashPassword(password);
String sqlString = "SELECT * FROM db_user WHERE username = '"
+ username +
"' AND password = '" + pwd + "'";
Statement stmt = connection.createStatement();
ResultSet rs = stmt.executeQuery(sqlString);
if (!rs.next()) {
throw new SecurityException(
"User name or password incorrect"
);
}
// Authenticated; proceed
} finally {
try {
connection.close();
} catch (SQLException x) {
// Forward to handler
}
}
}
}
/*
Modul4 - Login.java
Komen : English
Contoh kode noncompliant ini menunjukkan JDBC kode untuk mengotentikasi pengguna ke sistem. The
sandi berlalu sebagai char array, koneksi database dibuat, dan kemudian
password hashed.
Sayangnya, contoh kode ini memungkinkan serangan injeksi SQL karena SQL
pernyataan sqlString menerima argumen masukan unsanitized. Skenario serangan yang diuraikan
sebelumnya akan bekerja seperti yang dijelaskan.
*/
    

