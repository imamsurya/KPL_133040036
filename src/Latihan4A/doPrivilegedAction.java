/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4A;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class doPrivilegedAction {
    String username, char[] password
) throws SQLException {
Connection connection = getConnection();
if (connection == null) {
// Handle error
}
try {
String pwd = hashPassword(password);
// Validate username length
if (username.length() > 8) {
// Handle error
}
String sqlString =
"select * from db_user where username=? and password=?";
PreparedStatement stmt = connection.prepareStatement(sqlString);
stmt.setString(1, username);
stmt.setString(2, pwd);
ResultSet rs = stmt.executeQuery();
if (!rs.next()) {
throw new SecurityException("User name or password incorrect");
}
// Authenticated; proceed
}       catch (SQLException ex) {
            Logger.getLogger(doPrivilegedAction.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
try {
connection.close();
} catch (SQLException x) {
// Forward to handler
}
}
}

    private Connection getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String hashPassword(char[] password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/*
Komen :
Solusi ini compliant memodifikasi metode doPrivilegedAction() menggunakan PreparedStatement
bukan java.sql.Statement. Kode ini juga memvalidasi panjang
username argumen, mencegah penyerang dari mengirimkan nama pengguna sewenang-wenang panjang.
Menggunakan metode set*() kelas PreparedStatement untuk menegakkan memeriksa jenis kuat.
Ini mitigates SQL Injeksi kerentanan karena input benar melarikan diri dengan
otomatis jebakan dalam tanda kutip ganda. Catatan bahwa pernyataan siap harus digunakan
bahkan dengan permintaan yang memasukkan data ke dalam database.
*/