/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4B;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author ACER
 */
public class Format {
    class Format {
static Calendar c =
new GregorianCalendar(1995, GregorianCalendar.MAY, 23);
public static void main(String[] args) {
// args[0] is the credit card expiration date
// Perform comparison with c,
// if it doesn't match, print the following line
System.out.format(
"%s did not match! HINT: It was issued on %terd of some month",
args[0], c
);
}
}
}
/*
Modul4 - Format.java
Komen :
Contoh kode noncompliant ini menunjukkan masalah kebocoran informasi. It menerima
tanggal kadaluarsa kartu kredit sebagai argumen masukan dan menggunakannya dalam format string.
Dalam ketiadaan validasi input yang tepat, penyerang dapat menentukan tanggal terhadap
yang input yang diverifikasi oleh memasok masukan yang termasuk salah satu format
string argumen %1$ tm, %1$ te, atau %1$ tY.
memastikan bahwa pengguna yang dihasilkan masukan yang dikecualikan dari format string dan
Memungkinkan input pengguna untuk noda format string dapat menyebabkan kebocoran informasi atau penolakan layanan.
*/
