/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4C;

import java.io.InputStream;

/**
 *
 * @author ACER
 */
public class DirList {
    public static void main(String[] args) throws Exception {
String dir = System.getProperty("dir");
Runtime rt = Runtime.getRuntime();
Process proc = rt.exec("cmd.exe /C dir " + dir);
int result = proc.waitFor();
if (result != 0) {
System.out.println("process error: " + result);
}
InputStream in = (result == 0) ? proc.getInputStream() :
proc.getErrorStream();
int c;
while ((c = in.read()) != -1) {
System.out.print((char) c);
}
}
}
/*
Modul4 - Dirlist.java
komen :
Contoh kode noncompliant ini menyediakan daftar direktori menggunakan perintah dir. Ini
diimplementasikan menggunakan Runtime.exec() untuk memanggil perintah dir Windows.
Karena Runtime.exec() menerima unsanitized data yang berasal dari lingkungan,
kode ini rentan terhadap serangan injeksi perintah.
Penyerang dapat memanfaatkan program ini menggunakan perintah berikut:
Jawa - Ddir = 'dummy & echo buruk' Java
Perintah dijalankan adalah sebenarnya dua perintah:
dir/c CMD.exe dummy & echo buruk
upaya yang pertama untuk daftar tidak ada dummy folder dan kemudian cetakan buruk untuk konsol
*/