/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4D;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ACER
 */
public class LogSearch {
public static void FindLogEntry(String search) {
// Construct regex dynamically from user string
String regex = "(.*? +public\\[\\d+\\] +.*" + search + ".*)";
Pattern searchPattern = Pattern.compile(regex);
try (FileInputStream fis = new FileInputStream("log.txt")) {
FileChannel channel = fis.getChannel();
// Get the file's size and map it into memory
long size = channel.size();
final MappedByteBuffer mappedBuffer = channel.map(
FileChannel.MapMode.READ_ONLY, 0, size);
Charset charset = Charset.forName("ISO-8859-15");
final CharsetDecoder decoder = charset.newDecoder();
// Read file into char buffer
CharBuffer log = decoder.decode(mappedBuffer);
Matcher logMatcher = searchPattern.matcher(log);
while (logMatcher.find()) {
String match = logMatcher.group();
if (!match.isEmpty()) {
System.out.println(match);
}
}
} catch (IOException ex) {
System.err.println("thrown exception: " + ex.toString());
Throwable[] suppressed = ex.getSuppressed();
for (int i = 0; i < suppressed.length; i++) {
System.err.println("suppressed exception: "
+ suppressed[i].toString());
}
}
return;
}
    
}

/*
Modul4 - Logsearch.java
Komen :
Metode normalize() mengubah teks Unicode menjadi setara terdiri atau
bentuk diurai, memungkinkan untuk memudahkan pencarian teks. Metode Normalisasikanlah mendukung
bentuk-bentuk standar normalisasi yang dijelaskan dalam Unicode standar Annex #15-Unicode
Bentuk normalisasi.
Solusi ini menormalkan string sebelum memvalidasi itu. Representasi alternatif
string yang dinormalisasi dengan kanonik tanda kurung sudut. Akibatnya, masukan
validasi dengan benar mendeteksi input berbahaya dan melempar IllegalStateException.
*/