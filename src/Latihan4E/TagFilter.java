/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4E;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ACER
 */
public class TagFilter {
    public static String filterString(String str) {
String s = Normalizer.normalize(str, Form.NFKC);
// Validate input
Pattern pattern = Pattern.compile("<script>");
Matcher matcher = pattern.matcher(s);
if (matcher.find()) {
throw new IllegalArgumentException("Invalid input");
}
// Deletes noncharacter code points
s = s.replaceAll("[\\p{Cn}]", "");
return s;
}
public static void main(String[] args) {
// "\uFDEF" is a noncharacter code point
String maliciousInput = "<scr" + "\uFDEF" + "ipt>";
String sb = filterString(maliciousInput);
// sb = "<script>"
}
}
