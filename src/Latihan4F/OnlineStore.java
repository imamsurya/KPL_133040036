/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4F;

import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 *
 * @author ACER
 */
public class OnlineStore {
    private static void createXMLStreamBad(final BufferedOutputStream outStream,
final String quantity) throws IOException {
String xmlString = "<item>\n<description>Widget</description>\n"
+ "<price>500</price>\n" + "<quantity>" + quantity
+ "</quantity></item>";
outStream.write(xmlString.getBytes());
outStream.flush();
}
}

/*
Modul4 - OnlineStore.java
Komen :
Snipet kode XML dari sebuah toko online aplikasi, yang dirancang
terutama untuk query database back-end. Pengguna memiliki kemampuan untuk menentukan jumlah
item yang tersedia untuk pembelian.
*/
