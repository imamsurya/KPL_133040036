/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4G;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import jdk.internal.org.xml.sax.SAXException;
import jdk.internal.org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ACER
 */
public class XXE {
    private static void receiveXMLStream(InputStream inStream,
DefaultHandler defaultHandler)
throws ParserConfigurationException, SAXException, IOException, org.xml.sax.SAXException {
SAXParserFactory factory = SAXParserFactory.newInstance();
SAXParser saxParser = factory.newSAXParser();
//saxParser.parse(inStream, defaultHandler);
}
public static void main(String[] args) throws ParserConfigurationException,
SAXException, IOException, org.xml.sax.SAXException {
try {
receiveXMLStream(new FileInputStream("evil.xml"), new DefaultHandler());
} catch (java.net.MalformedURLException mue) {
System.err.println("Malformed URL Exception: " + mue);
}
}
}
 
/*
Modul4 - XXE.java
Komen :
Skema ini tersedia sebagai file schema.xsd. Solusi ini compliant mempekerjakan ini
skema untuk mencegah injeksi XML dari berhasil. Ia juga bergantung pada CustomResolver
serangan kelas untuk mencegah XXE. Kelas ini, serta XXE serangan, dijelaskan dalam berikutnya
contoh kode.
Menggunakan skema atau DTD untuk memvalidasi XML nyaman ketika menerima XML yang mungkin
telah dimuat dengan unsanitized masukan. Jika seperti string XML tidak belum telah dibangun, sanitasi
masukan sebelum membangun XML menghasilkan kinerja yang lebih baik.
*/