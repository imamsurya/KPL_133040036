/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4H;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author ACER
 */
public class TempFile {
        public static void main(String[] args) throws IOException {
Path tempFile = null;
try {
tempFile = Files.createTempFile("tempnam", ".tmp");
try (BufferedWriter writer =
Files.newBufferedWriter(tempFile, Charset.forName("UTF8"),
StandardOpenOption.DELETE_ON_CLOSE)) {
// Write to file
}
System.out.println("Temporary file write done, file erased");
} catch (FileAlreadyExistsException x) {
System.err.println("File exists: " + tempFile);
} catch (IOException x) {
// Some other sort of failure, such as permissions.
System.err.println("Error creating temporary file: " + x);
}
}
}

/*
Modul4 - TempFile.java
Komen :
Untuk ini dan berikutnya kode contoh berasumsi bahwa file yang dibuat dalam direktori aman di
sesuai dengan aturan FIO00-J dan dibuat dengan izin akses tepat di kepatuhan
dengan aturan FIO01-J. Baik persyaratan dapat dikelola di luar JVM.
Contoh kode noncompliant ini gagal untuk menghapus file setelah selesai.
Contoh kode noncompliant ini memanggil metode File.createTempFile(), yang
menghasilkan nama file sementara yang unik didasarkan pada dua parameter, awalan dan ekstensi.
Ini adalah satu-satunya metode saat ini dirancang dan disediakan untuk memproduksi nama file yang unik,
Meskipun nama yang dihasilkan dapat dengan mudah diprediksi. Pembangkit angka acak dapat
digunakan untuk memproduksi awalan jika nama file acak yang diperlukan.
*/
