/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4I;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 *
 * @author ACER
 */
public final class InputLibrary {
    public static char getChar() throws EOFException, IOException {
BufferedInputStream in = new BufferedInputStream(System.in); // Wrapper
int input = in.read();
if (input == -1) {
throw new EOFException();
}
// Down casting is permitted because InputStream guarantees read() in range
// 0..255 if it is not -1
return (char) input;
}
public static void main(String[] args) {
try {
// Either redirect input from the console or use
// System.setIn(new FileInputStream("input.dat"));
System.out.print("Enter first initial: ");
char first = getChar();
System.out.println("Your first initial is " + first);
System.out.print("Enter last initial: ");
char last = getChar();
System.out.println("Your last initial is " + last);
} catch (EOFException e) {
System.err.println("ERROR");
// Forward to handler
} catch (IOException e) {
System.err.println("ERROR");
// Forward to handler
}
}
}

/*
Modul4 - InputLibrary .java
Komen :
Contoh kode noncompliant ini menciptakan beberapa pembungkus BufferedInputStream pada
System.in, meskipun ada hanya satu deklarasi BufferedInputStream. The
metode getChar() menciptakan BufferedInputStream baru setiap kali yang disebut. Data yang
membaca dari aliran yang mendasari dan ditempatkan dalam buffer selama pelaksanaan satu panggilan
tidak bisa diganti di sungai yang mendasari sehingga panggilan kedua memiliki akses ke sana. Akibatnya,
data yang tetap dalam buffer pada akhir pelaksanaan tertentu getChar()
kehilangan. Meskipun contoh kode noncompliant ini menggunakan BufferedInputStream, setiap buffered
Wrapper tidak aman; kondisi ini juga exploitasi ketika menggunakan Scanner, misalnya.
*/
