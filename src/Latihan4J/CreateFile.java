/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4J;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 *
 * @author ACER
 */
public class CreateFile {
    public static void main(String[] args)
throws FileNotFoundException {
final PrintStream out =
new PrintStream(new BufferedOutputStream(
new FileOutputStream("foo.txt")));
try {
out.println("hello");
} finally {
//try {
out.close();
//} catch (IOException x) {
// Handle error
}
}
//Runtime.getRuntime().exit(1);
}

/*
Modul4 - CreateFile.java
komen :
Contoh ini menciptakan sebuah file baru, output beberapa teks itu, dan tiba-tiba keluar menggunakan Runtime.
Exit(). Akibatnya, file bisa ditutup tanpa teks sebenarnya sedang ditulis.
Solusi ini sesuai menambahkan hook shutdown untuk menutup file. Kait ini dipanggil oleh
Runtime.Exit() dan disebut sebelum JVM dihentikan.
*/
