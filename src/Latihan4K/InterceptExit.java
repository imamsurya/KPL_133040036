/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4K;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author ACER
 */
public class InterceptExit {
    public static void main(String[] args)
throws FileNotFoundException, IOException {
InputStream in = null;
try {
in = new FileInputStream("file");
System.out.println("Regular code block");
// Abrupt exit such as ctrl + c key pressed
System.out.println("This never executes");
} finally {
if (in != null) {
try {
in.close(); // This never executes either
} catch (IOException x) {
// Handle error
}
}
}
}
}

/*
Modul4 - InterceptExit.java
Komen :
Ketika pengguna tegas keluar program, misalnya dengan menekan Ctrl c tombol atau dengan
menggunakan perintah kill, JVM berakhir tiba-tiba. Meskipun acara ini tidak dapat diambil,
program harus tetap melakukan operasi pembersihan wajib sebelum
keluar. Contoh kode noncompliant ini gagal untuk melakukannya.
Menggunakan metode addShutdownHook() java.lang.Runtime untuk membantu dengan melakukan
pembersihan operasi dinonaktifkan tiba-tiba. JVM mulai shutdown hook
benang ketika tiba-tiba penghentian memprakarsai; kait penutupan berjalan concurrently dengan
benang JVM lainnya
*/