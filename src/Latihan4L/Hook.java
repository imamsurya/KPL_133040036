/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4L;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Hook {
    public static void main(String[] args) {
try {
final InputStream in = new FileInputStream("file");
Runtime.getRuntime().addShutdownHook(new Thread() {
public void run() {
    try {
        // Log shutdown and close all resources
        in.close();
    } catch (IOException ex) {
        Logger.getLogger(Hook.class.getName()).log(Level.SEVERE, null, ex);
    }
}
});
// ...
} catch (IOException x) {
// Handle error
//} catch (FileNotFoundException x) {
// Handle error
}
}
}

/*
Modul4 - Hook.java
Komen :
Untuk menghindari kondisi lomba atau kebuntuan antara tindakan shutdown, mungkin lebih baik untuk menjalankan
serangkaian shutdown tugas dari satu thread dengan menggunakan hook tunggal shutdown [Goetz 2006a].
Solusi ini compliant menunjukkan metode standar untuk menginstal sebuah hook.
JVM dapat membatalkan untuk alasan eksternal, seperti sinyal SIGKILL eksternal (POSIX) atau
TerminateProcess() panggilan (Windows), atau memori korupsi disebabkan oleh metode asli.
Kait penutupan mungkin gagal untuk melaksanakan seperti yang diharapkan dalam kasus tersebut karena JVM tidak bisa
menjamin bahwa mereka akan dilaksanakan sebagaimana dimaksud.
*/