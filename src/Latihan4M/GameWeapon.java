/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4M;

/**
 *
 * @author ACER
 */
public class GameWeapon {
    private static final long serialVersionUID = 24L;
int numOfWeapons = 10;
public String toString() {
return String.valueOf(numOfWeapons);
}
}

/*
Modul4 - GameWeapon.java
Komen :
Contoh kode noncompliant ini menerapkan kelas GameWeapon dengan bidang Defender
disebut numOfWeapons dan menggunakan bentuk serialisasi default. Setiap perubahan internal
perwakilan kelas dapat mematahkan bentuk serial yang sudah ada.
Karena kelas ini tidak memberikan serialVersionUID, Java Virtual Machine
(JVM) menetapkan itu salah satu yang menggunakan metode pelaksanaan-didefinisikan. Jika definisi kelas perubahan,
serialVersionUID ini juga kemungkinan akan berubah. Akibatnya, JVM akan menolak untuk mengasosiasikan
bentuk serial objek dengan definisi kelas ketika versi id
berbeda.
*/
