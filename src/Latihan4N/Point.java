/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4N;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author ACER
 */
public class Point implements Serializable {
    private transient double x; // Declared transient
private transient double y; // Declared transient
public Point(double x, double y) {
this.x = x;
this.y = y;
}
public Point() {
// No-argument constructor
}
}
/*public class Coordinates extends Point {
public static void main(String[] args) {
FileOutputStream fout = null;
try {
Point p = new Point(5,2);
fout = new FileOutputStream("point.ser");
ObjectOutputStream oout = new ObjectOutputStream(fout);
oout.writeObject(p);
oout.close();
} catch (Exception e) {
// Forward to handler
} finally {
if (fout != null) {
try {
fout.close();
} catch (IOException x) {
// Handle error
}
}
}
}
*/

/*
Modul4 - Point.java
Komen :
Data anggota kelas titik pribadi. Dengan asumsi koordinat sensitif, mereka
kehadiran di data stream akan mengekspos mereka untuk gangguan berbahaya.
antarmuka java.io.Serializable. Dengan demikian, kelas menunjukkan bahwa masalah keamanan
mungkin hasil dari objek serialisasi. Catatan bahwa subclass turunan apapun juga mewarisi ini
antarmuka dan akibatnya Defender. Pendekatan ini tidak pantas untuk kelas apapun yang
berisi data yang sensitif.
*/