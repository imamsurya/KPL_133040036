/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4O;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author ACER
 */
public class SensitiveClass extends Number {
    // ... Implement abstract methods, such as Number.doubleValue()â€¦
private static final SensitiveClass INSTANCE = new SensitiveClass();
public static SensitiveClass getInstance() {
return INSTANCE;
}
private SensitiveClass() {
// Perform security checks and parameter validation
}
private int balance = 1000;
protected int getBalance() {
return balance;
}

    @Override
    public int intValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long longValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float floatValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double doubleValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
class Malicious {
public static void main(String[] args) {
SensitiveClass sc =
(SensitiveClass) deepCopy(SensitiveClass.getInstance());
// Prints false; indicates new instance
System.out.println(sc == SensitiveClass.getInstance());
System.out.println("Balance = " + sc.getBalance());
}
// This method should not be used in production code
static public Object deepCopy(Object obj) {
try {
ByteArrayOutputStream bos = new ByteArrayOutputStream();
new ObjectOutputStream(bos).writeObject(obj);
ByteArrayInputStream bin =
new ByteArrayInputStream(bos.toByteArray());
return new ObjectInputStream(bin).readObject();
} catch (Exception e) {
throw new IllegalArgumentException(e);
}
}
}

/*
Modul4 - Malicious.java
Komen : 
Serialisasi dapat digunakan jahat, misalnya, untuk kembali beberapa contoh dari tunggal
kelas obyek. Dalam contoh noncompliant kode ini (berdasarkan [Bloch 2005a]), sebuah subclass
SensitiveClass secara tidak sengaja menjadi Defender karena java.lang.
Kelas, yang mengimplementasikan Serializable.
*/