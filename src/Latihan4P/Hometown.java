/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4P;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.AccessDeniedException;

/**
 *
 * @author ACER
 */
public final class Hometown implements Serializable {
    // Private internal state
private String town;
private static final String UNKNOWN = "UNKNOWN";
void performSecurityManagerCheck() throws AccessDeniedException {
// ...
}
void validateInput(String newCC) throws InvalidInputException {
// ...
}
public Hometown() throws AccessDeniedException {
performSecurityManagerCheck();
// Initialize town to default value
town = UNKNOWN;
}
// Allows callers to retrieve internal state
String getValue() throws AccessDeniedException {
performSecurityManagerCheck();
return town;
}
// Allows callers to modify (private) internal state
public void changeTown(String newTown) throws AccessDeniedException, InvalidInputException {
if (town.equals(newTown)) {
// No change
return;
} else {
performSecurityManagerCheck();
validateInput(newTown);
town = newTown;
}
}
private void writeObject(ObjectOutputStream out) throws IOException {
out.writeObject(town);
}
private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException, InvalidInputException {
in.defaultReadObject();
// If the deserialized name does not match the default value normally
// created at construction time, duplicate the checks
if (!UNKNOWN.equals(town)) {
validateInput(town);
}
}
}

/*
Modul4 - Hometown.java
Komen :
Dalam contoh noncompliant kode ini, pemeriksaan manajer keamanan yang digunakan dalam konstruktor
Tapi dihilangkan dari yang writeObject() dan readObject() metode yang digunakan dalam
proses serialisasi-deserialization. Kelalaian ini memungkinkan kode yang tidak dipercaya untuk jahat
membuat instance kelas.
Meskipun pemeriksaan manajer keamanan, data dalam contoh ini tidak sensitif. Serializing
data tidak terenkripsi, sensitif melanggar aturan SER03-J.
AccessDeniedException dan InvalidInputException yang kedua pengecualian keamanan
yang dapat dibuang oleh metode apapun tanpa memerlukan deklarasi melempar.
*/