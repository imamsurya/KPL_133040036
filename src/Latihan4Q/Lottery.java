/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4Q;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.security.SecureRandom;

/**
 *
 * @author ACER
 */
public class Lottery implements Serializable {
private int ticket = 1;
private SecureRandom draw = new SecureRandom();
public Lottery(int ticket) {
this.ticket = (int) (Math.abs(ticket % 20000) + 1);
}
public int getTicket() {
return this.ticket;
}
public int roll() {
this.ticket = (int) ((Math.abs(draw.nextInt()) % 20000) + 1);
return this.ticket;
}
public static void main(String[] args) {
Lottery l = new Lottery(2);
for (int i = 0; i < 10; i++) {
l.roll();
System.out.println(l.getTicket());
}
}
private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
in.defaultReadObject();
}
}

/*
Modul4 - Lottery.java
komen :
Contoh kode noncompliant ini menggunakan metode readObject() terdefinisi tapi gagal
melakukan validasi input setelah deserialization. Desain sistem membutuhkan maksimum
nomor tiket tiket lotere apapun menjadi 20.000. Namun, seorang penyerang dapat memanipulasi
array serial untuk menghasilkan jumlah yang berbeda pada deserialization.
Setiap masukan validasi dilakukan dalam konstruktor harus juga dilaksanakan dimanapun
objek dapat deserialized. Solusi ini compliant melakukan validasi lapangan-oleh-field
membaca segala bidang objek menggunakan metode readFields() dan ObjectInputStream.
GetField() konstruktor. Nilai setiap bidang harus sepenuhnya divalidasi sebelum diberikan
ke objek dalam pembangunan. Untuk lebih rumit invariants, ini mungkin membutuhkan membaca
beberapa bidang nilai ke variabel lokal agar cek yang bergantung pada kombinasi
nilai-nilai bidang.
*/