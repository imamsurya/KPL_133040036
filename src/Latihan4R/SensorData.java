/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4R;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author ACER
 */
class SensorData implements Serializable {

    static boolean isAvailable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
// 1 MB of data per instance!

//public static SensorData readSensorData() {...return (null);

    static SensorData readSensorData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
//public static boolean isAvailable() {...return false;
//}
//}
class SerializeSensorData {
public static void main(String[] args) throws IOException {
ObjectOutputStream out = null;
try {
out = new ObjectOutputStream(
new BufferedOutputStream(new FileOutputStream("ser.dat")));
while (SensorData.isAvailable()) {
// Note that each SensorData object is 1 MB in size
SensorData sd = SensorData.readSensorData();
out.writeObject(sd);
}
} finally {
if (out != null) {
out.close();
}
}
}
    
}

/*
Modul4 - SensorData.java
komen :
Contoh kode noncompliant ini membaca dan serializes data dari sensor eksternal. Masing-masing
doa metode readSensorData() kembali sebuah instance SensorData yang baru dibuat,
masing-masing berisi satu megabyte data. SensorData contoh yang aliran data murni, yang mengandung
data dan array namun kurang referensi objek SensorData lainnya.
Seperti telah dijelaskan, ObjectOutputStream mempertahankan cache ditulis sebelumnya
objek. Akibatnya, semua objek SensorData tetap hidup sampai cache itu sendiri menjadi
sampah yang dikumpulkan. Hal ini dapat mengakibatkan OutOfMemoryError karena aliran tetap
membuka sementara objek baru sedang menulis untuk itu.
*/