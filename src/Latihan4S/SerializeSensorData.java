/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Latihan4S;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author ACER
 */
public class SerializeSensorData {
    public static void main(String[] args) throws IOException {
ObjectOutputStream out = null;
try {
out = new ObjectOutputStream(
new BufferedOutputStream(new FileOutputStream("ser.dat")));
while (SensorData.isAvailable()) {
// Note that each SensorData object is 1 MB in size
SensorData sd = SensorData.readSensorData();
out.writeObject(sd);
out.reset(); // Reset the stream
}
} finally {
if (out != null) {
out.close();
}
}
}
}

/*
Modul4 - SerializeSensorData.java
Komen :
Contoh kode noncompliant ini membaca dan serializes data dari sensor eksternal. Masing-masing
doa metode readSensorData() kembali sebuah instance SensorData yang baru dibuat,
masing-masing berisi satu megabyte data. SensorData contoh yang aliran data murni, yang mengandung
data dan array namun kurang referensi objek SensorData lainnya.
Seperti telah dijelaskan, ObjectOutputStream mempertahankan cache ditulis sebelumnya
objek. Akibatnya, semua objek SensorData tetap hidup sampai cache itu sendiri menjadi
sampah yang dikumpulkan. Hal ini dapat mengakibatkan OutOfMemoryError karena aliran tetap
membuka sementara objek baru sedang menulis untuk itu.
Solusi compliant ini mengambil keuntungan dari sifat yang dikenal dari sensor data dengan ulang
aliran Keluaran setelah masing-masing menulis. Reset membersihkan aliran Keluaran objek internal
cache; Akibatnya, cache tidak lagi memelihara referensi ke sebelumnya ditulis Sensor-
Objek data. Kolektor sampah dapat mengumpulkan contoh SensorData yang tidak lagi diperlukan.
*/
